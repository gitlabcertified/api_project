#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:3.1 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build
WORKDIR /src
COPY ["aks_project/aks_project.csproj", "aks_project/"]
RUN dotnet restore "aks_project/aks_project.csproj"
COPY . .
WORKDIR "/src/aks_project"
RUN dotnet build "aks_project.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "aks_project.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "aks_project.dll"]